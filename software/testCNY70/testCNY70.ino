const int sensor_location = A0;
const int sensor_end = A1;
const int sensor_color = A2;

const int motor_enable = 7;
const int motor_a = 2;
const int motor_b = 3;

void motor_cw() {
  digitalWrite(motor_a, 0);
  digitalWrite(motor_b, 1);
  digitalWrite(motor_enable, 1);
}

void motor_ccw() {
  digitalWrite(motor_a, 1);
  digitalWrite(motor_b, 0);
  digitalWrite(motor_enable, 1);
}

void motor_stop() {
  digitalWrite(motor_enable, 0);
}

void setup() {
  Serial.begin(115200);

  pinMode(motor_enable, OUTPUT);
  pinMode(motor_a, OUTPUT);
  pinMode(motor_b, OUTPUT);
  motor_stop();
}

int sensor_location_value;
int sensor_end_value;
int sensor_color_value;

int serial_data;
int print_en;

void loop() {
  sensor_location_value = analogRead(sensor_location);
  sensor_end_value = analogRead(sensor_end);
  sensor_color_value = analogRead(sensor_color);

  if (print_en) {
    Serial.print(sensor_location_value);
    Serial.print(" ");
    Serial.print(sensor_end_value);
    Serial.print(" ");
    Serial.println(sensor_color_value);
  }

  if (Serial.available() > 0)
    serial_data = Serial.read();
  else
    serial_data = 0;

  if (serial_data == 's') print_en = !print_en;
  else if (serial_data == '1') motor_stop();
  else if (serial_data == '2') motor_cw();
  else if (serial_data == '3') motor_ccw();

  delay(10);
}
