const int sensor_location = A0;
const int sensor_end = A1;
const int sensor_color = A2;

const int motor_enable = 7;
const int motor_a = 2;
const int motor_b = 3;

const int sensor_set_th = 300;
const int sensor_reset_th = 700;

const int color_white = 600;
const int color_red = 760;
const int color_blue = 900;
const int color_empty = 970;

int sensor_location_value = 0;
int sensor_end_value = 0;
int sensor_location_state = 0;
int sensor_end_state = 0;
int sensor_location_last_state = 0;
int sensor_end_last_state = 0;
int sensor_color_value = 0;
int serial_data = 0;
int hole_number = 0;
int hole_set = 1;

int wait_counter = 10;

int print_en = 0;
int print_color = 0;

enum state {INIT, WAIT, RELEASE, BACK, TAKE, EMPTY};
enum state s = INIT;

void motor_cw() {
  digitalWrite(motor_a, 0);
  digitalWrite(motor_b, 1);
  digitalWrite(motor_enable, 1);
}

void motor_ccw() {
  digitalWrite(motor_a, 1);
  digitalWrite(motor_b, 0);
  digitalWrite(motor_enable, 1);
}

void motor_stop() {
  digitalWrite(motor_enable, 0);
}

int sensor_state(int value, int last_state) {
  int state;

  state = last_state;
  if (value > sensor_reset_th)
    state = 0;
  else if(value < sensor_set_th)
    state = 1;

  return state;
}

void setup() {
  Serial.begin(115200);

  pinMode(motor_enable, OUTPUT);
  pinMode(motor_a, OUTPUT);
  pinMode(motor_b, OUTPUT);
  motor_stop();
}

void loop() {
  sensor_location_value = analogRead(sensor_location);
  sensor_end_value = analogRead(sensor_end);
  sensor_color_value = analogRead(sensor_color);

  sensor_location_state = sensor_state(sensor_location_value, sensor_location_state);
  sensor_end_state = sensor_state(sensor_end_value, sensor_end_state); 

  if (print_en) {
    Serial.print(sensor_location_value);
    Serial.print(" ");
    Serial.print(sensor_end_value);
    Serial.print(" ");
    Serial.print(sensor_color_value);
    Serial.print(" ");
    Serial.print(sensor_location_state);
    Serial.print(" ");
    Serial.print(sensor_location_last_state);
    Serial.print(" ");
    Serial.print(sensor_end_state);
    Serial.print(" ");
    Serial.print(sensor_end_last_state);
    Serial.print(" ");
    Serial.print(hole_number);
    Serial.print(" ");
    Serial.print(hole_set);
    Serial.print(" ");
    Serial.println(s);
  }

  if (Serial.available() > 0)
    serial_data = Serial.read();
  else
    serial_data = 0;

  if (serial_data == 's') print_en = !print_en;
  else if (serial_data == 'c') print_color = !print_color;

  if (s == WAIT)
    hole_number = 0;
  else if (sensor_location_state == 1 && sensor_location_last_state == 0)
    hole_number++;

  if (s == WAIT) {
    hole_set = (sensor_color_value < color_white) ? 1 :
               (sensor_color_value < color_red)   ? 2 :
               (sensor_color_value < color_blue)  ? 3 :
                                                    4;
  }

  if (s == WAIT) wait_counter--;
  else wait_counter = 10;

  if (wait_counter == 0 && print_color) Serial.println(sensor_color_value);

  switch (s) {
    case INIT:
      s = (sensor_end_state == 1) ? WAIT : INIT;
      break;
    case WAIT:
      s = (wait_counter == 0) ? RELEASE : WAIT;
      break;
    case RELEASE:
      s = (hole_number == hole_set) ? BACK : RELEASE;
      break;
    case BACK:
      if (sensor_end_state == 0 && sensor_end_last_state == 1)
        s = (sensor_color_value > color_empty) ? EMPTY : TAKE;
      else
        s = BACK;
      break;
    case EMPTY:
      s = (sensor_color_value > color_empty) ? EMPTY : TAKE;
      break;
    case TAKE:
      s = (sensor_end_state == 0 && sensor_end_last_state == 1) ? WAIT : TAKE;
      break;
  }  

  switch (s) {
    case INIT:
      motor_cw();
      break;
    case WAIT:
      motor_stop();
      break;
    case RELEASE:
      motor_cw();
      break;
    case BACK:
      motor_ccw();
      break;
    case TAKE:
      motor_cw();
      break;
    case EMPTY:
      motor_stop();
      break;
  }

  sensor_location_last_state = sensor_location_state;
  sensor_end_last_state = sensor_end_state;
  delay(10);
}
